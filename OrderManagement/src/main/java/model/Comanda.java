package model;

public class Comanda {
	private int id_comanda;
	private String destinatar;
	private int valoare;

	public Comanda(int id_comanda, String destinatar, int valoare) {
		this.id_comanda = id_comanda;
		this.destinatar = destinatar;
		this.valoare = valoare;
	}

	public int getId_comanda() {
		return id_comanda;
	}

	public void setId_comanda(int id_comanda) {
		this.id_comanda = id_comanda;
	}

	public String getDestinatar() {
		return destinatar;
	}

	public void setDestinatar(String destinatar) {
		this.destinatar = destinatar;
	}

	public int getValoare() {
		return valoare;
	}

	public void setValoare(int valoare) {
		this.valoare = valoare;
	}
}

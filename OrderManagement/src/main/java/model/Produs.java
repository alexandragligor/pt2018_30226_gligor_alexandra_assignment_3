package model;

public class Produs {
	private int id_produs;
	private String nume;
	private int valabilitate;
	private int cantitate;

	public Produs(int id_produs, String nume, int valabilitate, int cantitate) {
		this.id_produs = id_produs;
		this.nume = nume;
		this.valabilitate = valabilitate;
		this.cantitate = cantitate;
	}

	public int getId_produs() {
		return id_produs;
	}

	public void setId_produs(int id_produs) {
		this.id_produs = id_produs;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public int getValabilitate() {
		return valabilitate;
	}

	public void setValabilitate(int valabilitate) {
		this.valabilitate = valabilitate;
	}

	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
}

package model;

public class Client {
	private int id_client;
	private String nume;
	private String cnp;
	private String email;

	public Client(int id_client, String nume, String cnp, String email) {
		this.id_client = id_client;
		this.nume = nume;
		this.cnp = cnp;
		this.email = email;
	}

	public int getId_client() {
		return id_client;
	}

	public void setId_client(int id_client) {
		this.id_client = id_client;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}

package presentation;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.Comanda;

public class OrderTableModel extends AbstractTableModel {
	
	private static final long serialVersionUID = -7718105788157604140L;
	
	private String[] columnNames = {"Id",
        "Destinatar",
        "Valoare"};
    private List<Comanda> comenzi;

    public OrderTableModel(List<Comanda> comenzi) {
    	this.comenzi = comenzi;
    }
    
    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return comenzi.size();
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
    	String value = "";
    	
    	switch (col) {
    	case 0:
    		value = String.valueOf(comenzi.get(row).getId_comanda());
    		break;
    	case 1:
    		value = comenzi.get(row).getDestinatar();
    		break;
    	case 2:
    		value = String.valueOf(comenzi.get(row).getValoare());
    		break;
    	}
    	
        return value;
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

}
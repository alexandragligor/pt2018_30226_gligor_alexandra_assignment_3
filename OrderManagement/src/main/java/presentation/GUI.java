package presentation;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import dao.ClientDAO;
import dao.ComandaDAO;
import dao.ProdusDAO;
import model.Client;
import model.Comanda;
import model.Produs;

public class GUI {

	private JFrame frame;

	private JPanel tabPanel;
	private JButton clientWindowButton;
	private JButton productWindowButton;
	private JButton orderWindowButton;

	private JPanel clientWindow;
	private JLabel idLabel;
	private JLabel nameLabel;
	private JLabel cnpLabel;
	private JLabel emailLabel;
	private JTextField idField;
	private JTextField nameField;
	private JTextField emailField;
	private JTextField cnpField;
	private JButton addClientButton;
	private JButton editClientButton;
	private JButton deleteClientButton;
	private JTable table;

	private JPanel productWindow;
	private JLabel id_pLabel;
	private JLabel name_pLabel;
	private JLabel valLabel;
	private JLabel cantLabel;
	private JTextField id_pField;
	private JTextField name_pField;
	private JTextField valField;
	private JTextField cantField;
	private JButton addProductButton;
	private JButton editProductButton;
	private JButton deleteProductButton;
	private JTable table_p;

	private JPanel orderWindow;
	private JLabel id_comandaLabel;
	private JLabel destinatarLabel;
	private JLabel valoareLabel;
	private JTextField id_comandaField;
	private JTextField destinatarField;
	private JTextField valoareField;
	private JButton addOrderButton;
	private JButton editOrderButton;
	private JButton deleteOrderButton;
	private JTable table_o;

	public GUI() {
		frame = new JFrame("OM");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 800);
		frame.setResizable(false);
		frame.setLayout(null);

		initTabPanel();
		initClientWindow();
		initProductWindow();
		initOrderWindow();

		showClientWindow();
		showProductWindow();
		showOrderWindow();

		frame.setVisible(true);
	}

	private void showClientWindow() {
		clearFrame();
		frame.add(clientWindow);
		frame.revalidate();
		frame.repaint();
	}

	private void showProductWindow() {
		clearFrame();
		frame.add(productWindow);
		frame.revalidate();
		frame.repaint();
	}

	private void showOrderWindow() {
		clearFrame();
		frame.add(orderWindow);
		frame.revalidate();
		frame.repaint();
	}

	private void initTabPanel() {
		tabPanel = new JPanel();
		tabPanel.setBounds(0, 0, 800, 40);
		tabPanel.setLayout(null);
		tabPanel.setBackground(Color.BLACK);

		clientWindowButton = new JButton("Clients");
		clientWindowButton.setBounds(5, 0, 100, 40);
		clientWindowButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showClientWindow();
			}
		});

		productWindowButton = new JButton("Products");
		productWindowButton.setBounds(110, 0, 100, 40);
		productWindowButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showProductWindow();
			}
		});

		orderWindowButton = new JButton("Orders");
		orderWindowButton.setBounds(215, 0, 100, 40);
		orderWindowButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showOrderWindow();
			}
		});

		tabPanel.add(clientWindowButton);
		tabPanel.add(productWindowButton);
		tabPanel.add(orderWindowButton);

		frame.add(tabPanel);
	}

	private void clearFrame() {
		frame.getContentPane().removeAll();
		frame.add(tabPanel);
	}

	private void initOrderWindow() {
		orderWindow = new JPanel();
		orderWindow.setLayout(null);
		orderWindow.setBackground(Color.BLUE);
		orderWindow.setBounds(0, 40, 800, 760);

		id_comandaLabel = new JLabel("Id");
		destinatarLabel = new JLabel("Destinatar");
		valoareLabel = new JLabel("Valoare");
		id_comandaField = new JTextField();
		destinatarField = new JTextField();
		valoareField = new JTextField();

		id_comandaLabel.setBounds(10, 10, 100, 40);
		id_comandaField.setBounds(10, 60, 200, 40);
		destinatarLabel.setBounds(10, 110, 100, 40);
		destinatarField.setBounds(10, 160, 200, 40);
		valoareLabel.setBounds(10, 210, 100, 40);
		valoareField.setBounds(10, 260, 200, 40);

		addOrderButton = new JButton("Add_c");
		addOrderButton.setBounds(10, 410, 100, 40);
		addOrderButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(id_comandaField.getText());
				String name = destinatarField.getText();
				int val = Integer.parseInt(valoareField.getText());

				Comanda comanda = new Comanda(id, name, val);

				ComandaDAO.insert(comanda);
				refreshTableComanda();
			}
		});
		editOrderButton = new JButton("Edit_c");
		editOrderButton.setBounds(10, 460, 100, 40);
		editOrderButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(id_comandaField.getText());
				String name = destinatarField.getText();
				int val = Integer.parseInt(valoareField.getText());

				Comanda comanda = new Comanda(id, name, val);

				ComandaDAO.update(comanda, id);
				refreshTableComanda();
			}
		});

		deleteOrderButton = new JButton("Delete");
		deleteOrderButton.setBounds(10, 510, 100, 40);
		deleteOrderButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(id_comandaField.getText());

				Comanda comanda = new Comanda(id, null, 0);

				ComandaDAO.delete(comanda);
				refreshTableComanda();
			}
		});

		table_o = new JTable();
		refreshTableComanda();
		table_o.setBounds(300, 10, 490, 740);

		orderWindow.add(id_comandaLabel);
		orderWindow.add(destinatarLabel);
		orderWindow.add(valoareLabel);
		orderWindow.add(id_comandaField);
		orderWindow.add(destinatarField);
		orderWindow.add(valoareField);
		orderWindow.add(addOrderButton);
		orderWindow.add(editOrderButton);
		orderWindow.add(deleteOrderButton);
		orderWindow.add(table_o);

	}

	private void initProductWindow() {
		productWindow = new JPanel();
		productWindow.setLayout(null);
		productWindow.setBackground(Color.GREEN);
		productWindow.setBounds(0, 40, 800, 760);

		id_pLabel = new JLabel("Id");
		name_pLabel = new JLabel("Nume");
		valLabel = new JLabel("Valabil");
		cantLabel = new JLabel("Cantitate");
		id_pField = new JTextField();
		name_pField = new JTextField();
		valField = new JTextField();
		cantField = new JTextField();

		id_pLabel.setBounds(10, 10, 100, 40);
		id_pField.setBounds(10, 60, 200, 40);
		name_pLabel.setBounds(10, 110, 100, 40);
		name_pField.setBounds(10, 160, 200, 40);
		valLabel.setBounds(10, 210, 100, 40);
		valField.setBounds(10, 260, 200, 40);
		cantLabel.setBounds(10, 310, 100, 40);
		cantField.setBounds(10, 360, 200, 40);

		addProductButton = new JButton("Add_p");
		addProductButton.setBounds(10, 410, 100, 40);
		addProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(id_pField.getText());
				String name = name_pField.getText();
				int val = Integer.parseInt(valField.getText());
				int cantitate = Integer.parseInt(cantField.getText());

				Produs produs = new Produs(id, name, val, cantitate);

				ProdusDAO.insert(produs);
				refreshTableProdus();
			}
		});
		editProductButton = new JButton("Edit_p");
		editProductButton.setBounds(10, 460, 100, 40);
		editProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(id_pField.getText());
				String name = name_pField.getText();
				int val = Integer.parseInt(valField.getText());
				int cantitate = Integer.parseInt(cantField.getText());

				Produs produs = new Produs(id, name, val, cantitate);

				ProdusDAO.update(produs, id);
				refreshTableProdus();
			}
		});

		deleteProductButton = new JButton("Delete");
		deleteProductButton.setBounds(10, 510, 100, 40);
		deleteProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(id_pField.getText());

				Produs produs = new Produs(id, null, 0, 0);

				ProdusDAO.delete(produs);
				refreshTableProdus();
			}
		});

		table_p = new JTable();
		refreshTableProdus();
		table_p.setBounds(300, 10, 490, 740);

		productWindow.add(id_pLabel);
		productWindow.add(name_pLabel);
		productWindow.add(valLabel);
		productWindow.add(cantLabel);
		productWindow.add(id_pField);
		productWindow.add(name_pField);
		productWindow.add(valField);
		productWindow.add(cantField);
		productWindow.add(addProductButton);
		productWindow.add(editProductButton);
		productWindow.add(deleteProductButton);
		productWindow.add(table_p);

	}

	private void initClientWindow() {
		clientWindow = new JPanel();
		clientWindow.setLayout(null);
		clientWindow.setBackground(Color.YELLOW);
		clientWindow.setBounds(0, 40, 800, 760);

		idLabel = new JLabel("Id");
		nameLabel = new JLabel("Name");
		cnpLabel = new JLabel("Cnp");
		emailLabel = new JLabel("Email");
		idField = new JTextField();
		nameField = new JTextField();
		cnpField = new JTextField();
		emailField = new JTextField();

		idLabel.setBounds(10, 10, 100, 40);
		idField.setBounds(10, 60, 200, 40);
		nameLabel.setBounds(10, 110, 100, 40);
		nameField.setBounds(10, 160, 200, 40);
		cnpLabel.setBounds(10, 210, 100, 40);
		cnpField.setBounds(10, 260, 200, 40);
		emailLabel.setBounds(10, 310, 100, 40);
		emailField.setBounds(10, 360, 200, 40);

		addClientButton = new JButton("Add");
		addClientButton.setBounds(10, 410, 100, 40);
		addClientButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(idField.getText());
				String name = nameField.getText();
				String cnp = cnpField.getText();
				String email = emailField.getText();

				Client client = new Client(id, name, cnp, email);

				ClientDAO.insert(client);
				refreshTable();
			}
		});
		editClientButton = new JButton("Edit");
		editClientButton.setBounds(10, 460, 100, 40);
		editClientButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(idField.getText());
				String name = nameField.getText();
				String cnp = cnpField.getText();
				String email = emailField.getText();

				Client client = new Client(id, name, cnp, email);

				ClientDAO.update(client, id);
				refreshTable();
			}
		});

		deleteClientButton = new JButton("Delete");
		deleteClientButton.setBounds(10, 510, 100, 40);
		deleteClientButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(idField.getText());

				Client client = new Client(id, null, null, null);

				ClientDAO.delete(client);
				refreshTable();

			}
		});

		table = new JTable();
		refreshTable();
		table.setBounds(300, 10, 490, 740);

		clientWindow.add(idLabel);
		clientWindow.add(nameLabel);
		clientWindow.add(cnpLabel);
		clientWindow.add(emailLabel);
		clientWindow.add(idField);
		clientWindow.add(nameField);
		clientWindow.add(cnpField);
		clientWindow.add(emailField);
		clientWindow.add(addClientButton);
		clientWindow.add(editClientButton);
		clientWindow.add(deleteClientButton);
		clientWindow.add(table);
	}

	private void refreshTable() {
		List<Client> clients = ClientDAO.select();
		table.setModel(new ClientTableModel(clients));
		frame.revalidate();
		frame.repaint();
	}

	private void refreshTableProdus() {
		List<Produs> produse = ProdusDAO.select();
		table_p.setModel(new ProductTableModel(produse));
		frame.revalidate();
		frame.repaint();
	}

	public void refreshTableComanda() {
		List<Comanda> comenzi = ComandaDAO.select();
		table_o.setModel(new OrderTableModel(comenzi));
		frame.revalidate();
		frame.repaint();
	}

}

package presentation;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.Client;

class ClientTableModel extends AbstractTableModel {
	
	private static final long serialVersionUID = -7718105788157604140L;
	
	private String[] columnNames = {"Id",
        "Nume",
        "Cnp",
        "Email"};
    private List<Client> clients;

    public ClientTableModel(List<Client> clients) {
    	this.clients = clients;
    }
    
    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return clients.size();
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
    	String value = "";
    	
    	switch (col) {
    	case 0:
    		value = String.valueOf(clients.get(row).getId_client());
    		break;
    	case 1:
    		value = clients.get(row).getNume();
    		break;
    	case 2:
    		value = clients.get(row).getCnp();
    		break;
    	case 3: 
    		value = clients.get(row).getEmail();
    		break;
    	}
    	
        return value;
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

}


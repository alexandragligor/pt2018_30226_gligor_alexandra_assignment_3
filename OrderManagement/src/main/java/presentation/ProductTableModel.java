package presentation;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.Produs;

class ProductTableModel extends AbstractTableModel {
	
	private static final long serialVersionUID = -7718105788157604140L;
	
	private String[] columnNames = {"Id",
        "Nume",
        "Valabilitate",
        "Cantitate"};
    private List<Produs> produse;

    public ProductTableModel(List<Produs> produse) {
    	this.produse = produse;
    }
    
    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return produse.size();
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
    	String value = "";
    	
    	switch (col) {
    	case 0:
    		value = String.valueOf(produse.get(row).getId_produs());
    		break;
    	case 1:
    		value = produse.get(row).getNume();
    		break;
    	case 2:
    		value = String.valueOf(produse.get(row).getValabilitate());
    		break;
    	case 3: 
    		value = String.valueOf(produse.get(row).getCantitate());
    		break;
    	}
    	
        return value;
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

}
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import data.ConnectionFactory;
import model.Produs;

public class ProdusDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProdusDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO produs (id_produs,nume,valabilitate,cantitate)"
			+ " VALUES (?,?,?,?)";
	private static final String deleteStatementString = "DELETE FROM produs WHERE id_produs=? ";
	private static final String updateStatementString = "UPDATE produs SET id_produs=?, nume=?, valabilitate=?, cantitate=? WHERE id_produs=?";
	private static final String selectAllStatementString = "SELECT * FROM produs";

	public static void insert(Produs produs) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, produs.getId_produs());
			insertStatement.setString(2, produs.getNume());
			insertStatement.setInt(3, produs.getValabilitate());
			insertStatement.setInt(4, produs.getCantitate());
			insertStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static void delete(Produs produs) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement delete = null;
		try {
			delete = dbConnection.prepareStatement(deleteStatementString);
			delete.setInt(1, produs.getId_produs());
			delete.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(delete);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static void update(Produs produs, int id) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setInt(1, produs.getId_produs());
			updateStatement.setString(2, produs.getNume());
			updateStatement.setInt(3, produs.getValabilitate());
			updateStatement.setInt(4, produs.getCantitate());
			updateStatement.setInt(5, id);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static ArrayList<Produs> select() {
		ArrayList<Produs> p = new ArrayList<Produs>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement Select = null;
		ResultSet rs = null;
		try {
			Select = dbConnection.prepareStatement(selectAllStatementString);
			rs = Select.executeQuery();
			while (rs.next()) {
				p.add(new Produs(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4)));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:select ALL " + e.getMessage());
		} finally {
			ConnectionFactory.close(Select);
			ConnectionFactory.close(dbConnection);
		}
		return p;
	}

}

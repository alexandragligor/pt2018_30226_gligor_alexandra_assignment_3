package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import data.ConnectionFactory;
import model.Comanda;

public class ComandaDAO {

	protected static final Logger LOGGER = Logger.getLogger(ComandaDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO comanda (id_comanda,destinatar,valoare)"
			+ " VALUES (?,?,?)";
	private static final String deleteStatementString = "DELETE FROM comanda WHERE id_comanda=? ";
	private static final String updateStatementString = "UPDATE comanda SET id_comanda=?, destinatar=?, valoare=? WHERE id_coamanda=?";
	private static final String selectAllStatementString = "SELECT * FROM comanda";

	public static void insert(Comanda comanda) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, comanda.getId_comanda());
			insertStatement.setString(2, comanda.getDestinatar());
			insertStatement.setInt(3, comanda.getValoare());
			insertStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	

	public static void delete(Comanda comanda) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement delete = null;
		try {
			delete = dbConnection.prepareStatement(deleteStatementString);
			delete.setInt(1, comanda.getId_comanda());
			delete.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ComandaDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(delete);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static void update(Comanda comanda, int id) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setInt(1, comanda.getId_comanda());
			updateStatement.setString(2, comanda.getDestinatar());
			updateStatement.setInt(3, comanda.getValoare());
			updateStatement.setInt(4, id);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ComandaDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static ArrayList<Comanda> select() {
		ArrayList<Comanda> c = new ArrayList<Comanda>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement Select = null;
		ResultSet rs = null;
		try {
			Select = dbConnection.prepareStatement(selectAllStatementString);
			rs = Select.executeQuery();
			while (rs.next()) {
				c.add(new Comanda(rs.getInt(1), rs.getString(2), rs.getInt(3)));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ComandaDAO:select ALL " + e.getMessage());
		} finally {
			ConnectionFactory.close(Select);
			ConnectionFactory.close(dbConnection);
		}
		return c;
	}

}

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import data.ConnectionFactory;
import model.Client;

public class ClientDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO client (id_client,nume,cnp,email)"
			+ " VALUES (?,?,?,?)";
	private static final String deleteStatementString = "DELETE FROM client WHERE id_client=? ";
	private static final String updateStatementString = "UPDATE client SET id_client=?, nume=?, cnp=?, email=? WHERE id_client=?";
	private static final String selectAllStatementString = "SELECT * FROM client";

	public static void insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString);
			insertStatement.setInt(1, client.getId_client());
			insertStatement.setString(2, client.getNume());
			insertStatement.setString(3, client.getCnp());
			insertStatement.setString(4, client.getEmail());
			insertStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static void delete(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement delete = null;
		try {
			delete = dbConnection.prepareStatement(deleteStatementString);
			delete.setInt(1, client.getId_client());
			delete.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(delete);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static void update(Client client, int id) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setInt(1, client.getId_client());
			updateStatement.setString(2, client.getNume());
			updateStatement.setString(3, client.getCnp());
			updateStatement.setString(4, client.getEmail());
			updateStatement.setInt(5, id);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static ArrayList<Client> select() {
		ArrayList<Client> c = new ArrayList<Client>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement Select = null;
		ResultSet rs = null;
		try {
			Select = dbConnection.prepareStatement(selectAllStatementString);
			rs = Select.executeQuery();
			while (rs.next()) {
				c.add(new Client(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:select ALL " + e.getMessage());
		} finally {
			ConnectionFactory.close(Select);
			ConnectionFactory.close(dbConnection);
		}
		return c;
	}

}
